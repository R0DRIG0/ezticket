from django.shortcuts import render
from Auction.models import *
from django.shortcuts import get_object_or_404
import time
def Subasta(request):
    template = 'subasta.html'
    data = {}
    data['auctions'] = Auction.objects.all()
    return render(request, template, data)

def Mis_ubasta(request):
    template = 'mis_subasta.html'
    data = {}
    user = request.user
    list_auctions = []
    for x in Auction.objects.all():
        if (user == x.user.user):
            list_auctions.append(x)
    data['auctions'] = list_auctions
    return render(request, template, data)

def Ofertar(request,auction_id):
    template = 'ofertar.html'
    data = {}
    auction_object = get_object_or_404(Auction,pk=auction_id)
    data['auction'] = auction_object
    if request.POST:
        price = auction_object.price_init
        auction_object.price_init = price + int(request.POST['tender'])
        auction_object.save()
        date_now = (time.strftime("%Y/%m/%d")).replace('/','-')
        Date_object = Date.objects.create(
                            date = date_now
                            )
        hour_object = Hour.objects.create(
                            hour = time.strftime("%H:%M:%S")
                            )
        Tender.objects.create(
                                boletus = auction_object.boletus,
                                date = Date_object,
                                user = request.user.userprofile,
                                price_add = request.POST['tender'],
                                hour = hour_object
                                )
    return render(request, template, data)
    