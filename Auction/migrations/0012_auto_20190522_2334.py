# Generated by Django 2.0.13 on 2019-05-22 23:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Auction', '0011_auto_20190522_2333'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auction',
            name='passage',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='Auction.Passage'),
        ),
    ]
