# Generated by Django 2.0.13 on 2019-05-21 03:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Auction', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='passage',
            name='date_departure',
            field=models.DateField(blank=True, null=True),
        ),
    ]
