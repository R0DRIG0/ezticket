from django.shortcuts import render
from Auction.models import *
from django.shortcuts import get_object_or_404
import qrcode
import time

# Create your views here.
#Example
#def example(request):
#    template = 'example.html'
#    data = {}
#    return render(request, template, data)

def tablePrint(request):
    template = 'tablePrint.html'
    data = {}
    user = request.user
    list_passage = []
    for x in Passage.objects.all():
        if (user == x.user.user):
            list_passage.append(x)
    data['passages'] = list_passage
    return render(request, template, data)

def Print(request,passage_id):
    template = 'print.html'
    data = {}
    
    passage_object = get_object_or_404(Passage,pk=passage_id)
    list_passage = [
            passage_object.pk, 
            passage_object.user.user.first_name,
            passage_object.station_departure,
            passage_object.destination,
            passage_object.company
            ]
    qr = qrcode.make(list_passage)
    qr.save('Static/img/Qr.png')
    data['passage'] = passage_object
    if request.POST:
        boletus_object = Boletus.objects.create()
        date_object = Date.objects.create(
                                            date = request.POST['date']
                                            )
        hour_object = Hour.objects.create(
                                            hour = request.POST['hour']
                                            )
        Auction.objects.create(
                                boletus = boletus_object,
                                user = request.user.userprofile,
                                passage = passage_object,
                                date = date_object,
                                price_init = request.POST['price'],
                                hour = hour_object,
                                hour_init = time.strftime("%H:%M:%S")
                                )
    return render(request, template, data)