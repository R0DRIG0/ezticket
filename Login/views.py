from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, logout
from django.contrib.auth.models import User
from django.contrib.auth import login as loginn
from django.urls import reverse
from Login.models import *
from django.core.mail import send_mail
import random
import string


def randomString(stringLength=4):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    global CODE
    CODE = ''.join(random.choice(letters) for i in range(stringLength))


# Create your views here.

def Login(request):
    template = 'login.html'
    data = {}
    logout(request)
    if request.POST:
        user = authenticate(username=request.POST['user'], password=request.POST['pass'])
        if user != None:
            loginn(request,user)
            return JsonResponse({'result':True})
        return JsonResponse({'result':False})
    return render(request, template, data)


def Register(request):
    template = 'register.html'
    data = {}
    if request.POST:
        for x in request.POST:
            if request.POST[x] == "":
                return JsonResponse({'result':False})

        for i in User.objects.all():
            if (request.POST['user'] == i.username):
                return JsonResponse({'result':'username'})

        if ("@" in request.POST['user']) == False:
            return JsonResponse({'result':False})
        

        object_user = User.objects.create_user(
                                            username = request.POST['user'],
                                            password = request.POST['pass'],
                                            first_name = request.POST['first_name'].capitalize(),
                                            last_name = request.POST['last_name'].capitalize(),
                                            is_active = True
                                        )
        Userprofile.objects.create(
                                    user = object_user,
                                    rut   = request.POST['rut'],
                                    phone = request.POST['phone'],
                                    discastatus = request.POST['discapacity']
                                )
        user = authenticate(username=request.POST['user'], password=request.POST['pass'])
        loginn(request,object_user)
        randomString()
        send_mail(
            'Validación del correo',
            'Su codigo de verificación: ' + CODE + '',
            'ezticketdev@gmail.com',
            [request.POST['user']],
            fail_silently=False
        )

        return JsonResponse({'result':True})

    return render(request, template, data)

def Email_password(request):
    template = 'email_password.html'
    data = {}
    if request.POST:
        print (request.POST)
        users = User.objects.all()
        for user in users:
            if request.POST['email'] == str(user):
                randomString()
                send_mail(
                    'Recuperar contraseña',
                    'Su codigo es: ' + CODE + '',
                    'ezticketdev@gmail.com',
                    [request.POST['email']],
                    fail_silently=False
                )
                global user_pass
                user_pass = user
                return JsonResponse({'result':True})
        return JsonResponse({'result':False})

    return render(request, template, data)

def Forgot_password(request):
    template = 'forgot_password.html'
    data = {}
    if request.POST:
        user_profile =User.objects.get(username=user_pass)
        user_profile.set_password(request.POST['pass'])
        user_profile.save()
        return JsonResponse({'result':True})
    return render(request, template, data)

def Validate(request):
    template = 'validate.html'
    data = {}
    if request.POST:
        user_profile =Userprofile.objects.get(user=request.user)
        if CODE == request.POST['code']:
            user_profile.verificate = True
            user_profile.save()
            return JsonResponse({'result':True})
        return JsonResponse({'result':False})

    return render(request, template, data)

def Validate_password(request):
    template = 'validate_password.html'
    data = {}
    if request.POST:
        print("1111111111111111111111")
        if CODE == request.POST['code']:
            print("22222222222222222222")
            return JsonResponse({'result':True})
        return JsonResponse({'result':False})
    return render(request, template, data)

def Dashboard(request):
    template = 'dashboard.html'
    data = {}
    return render(request, template, data)
    
